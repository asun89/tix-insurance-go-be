// Autogenerated by Typical-Go. DO NOT EDIT.

package dependency

import "github.com/typical-go/typical-rest-server/typical"

func init() {
	typical.Context.TestTargets.Append("./app")
	typical.Context.TestTargets.Append("./app/config")
	typical.Context.TestTargets.Append("./app/controller")
	typical.Context.TestTargets.Append("./app/repository")
	typical.Context.TestTargets.Append("./app/service")
}
